export const colors = {
  primary: '#424242',
  primaryDark: '#1b1b1b',
  primaryLight: '#6d6d6d',
  secondary: '#2196f3',
  secondaryDark: '#0069c0',
  secondaryLight: '#6ec6ff',
  error: '#F44336',
  success: '#4CAF50',
  white: '#FFFFFF'
};
