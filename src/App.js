import React, { Component } from 'react';
import { colors } from './assets/config';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

import Top from './components/top';
import Bottom from './components/bottom';

const { app } = window.require('electron').remote;
const { remote } = window.require('electron');

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: colors.primary,
    padding: 0,
    margin: 0,
    height: 600
  }
});

class App extends Component {


  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Top />
        <Bottom />
      </div>
    );
  }
}

export default withStyles(styles)(App);
