import React, { Component } from 'react';
import { colors } from '../assets/config';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import ReactCountryFlag from 'react-country-flag';
import iplocation from 'iplocation';
import Success from '../assets/img/greenCheck.png';
import Fail from '../assets/img/redx.png';

const { remote } = window.require('electron');

const styles = theme => ({
  root: {
    flexGrow: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  statusText: {
    color: colors.white,
    marginTop: 16
  },
  headerText: {
    color: colors.secondary
  },
  text: {
    color: colors.white
  },
  vpnImage: {
    width: 50,
    marginLeft: 100
  }
});

class Bottom extends Component {
  state = {
    ipLocation: null
  };

  getIPLocation = ip => {
    if (ip != 'Not Found') {
      iplocation(ip)
        .then(data => {
          console.log('Location Data: ', data);
          this.setState({
            ipLocation: data
          });
        })
        .catch(error => {
          console.log('Location Error: ', error);
          this.setState({
            ipLocation: null
          });
        });
    } else {
      this.setState({
        ipLocation: null
      });
    }
  };

  render() {
    const { classes } = this.props;
    const ip = remote.getGlobal('ip');
    const mac = remote.getGlobal('mac');
    const proxy = remote.getGlobal('proxy');
    const publicIp = remote.getGlobal('publicIp');
    const { ipLocation } = this.state;

    if (ipLocation == null) {
      this.getIPLocation(publicIp);
    }

    console.log('Proxy: ', proxy);
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={6}>
            <Typography className={classes.statusText} variant="h6">
              VPN status:
            </Typography>
          </Grid>
          <Grid item xs={6}>
            {proxy != 'Not Found' ? (
              <img className={classes.vpnImage} src={Success} />
            ) : (
              <img className={classes.vpnImage} src={Fail} />
            )}
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.headerText} variant="h6">
              Local Config
            </Typography>
            <Typography className={classes.text} variant="subtitle2">
              IP - {ip}
            </Typography>
            <Typography className={classes.text} variant="subtitle2">
              MAC - {mac}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.headerText} variant="h6">
              VPN Config
            </Typography>
            {proxy != 'Not Found' ? (
              <div>
                <Typography className={classes.text} variant="subtitle2">
                  IP - {proxy[0].ip}
                </Typography>
                <Typography className={classes.text} variant="subtitle2">
                  MAC - {proxy[0].mac}
                </Typography>
              </div>
            ) : (
              <div>
                <Typography className={classes.text} variant="subtitle2">
                  Not available
                </Typography>
              </div>
            )}
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.headerText} variant="h6">
              Location
            </Typography>
            {ipLocation != null &&
              ipLocation.data != 'undefined' && (
                <div>
                  <Typography className={classes.text} variant="subtitle2">
                    Country - {ipLocation.country}
                  </Typography>
                  <Typography className={classes.text} variant="subtitle2">
                    City - {ipLocation.city}
                  </Typography>
                  <Typography className={classes.text} variant="subtitle2">
                    Region - {ipLocation.region}
                  </Typography>
                </div>
              )}
          </Grid>
          {ipLocation != null &&
            ipLocation.data != 'undefined' && (
              <Grid item xs={6}>
                <ReactCountryFlag
                  code={ipLocation.countryCode}
                  svg
                  styleProps={{
                    width: '150px',
                    height: '65px',
                    marginTop: '25px'
                  }}
                />
              </Grid>
            )}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Bottom);
