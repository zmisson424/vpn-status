import React, { Component } from 'react';
import { colors } from '../assets/config';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

import Flag from '../assets/img/flag.gif';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  image: {
    width: 450
  },
  header: {
    color: colors.secondary,
    padding: 10,
    textAlign: 'center'
  }
});

class Top extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <img className={classes.image} src={Flag} />
        <Typography variant="h4" className={classes.header}>
          VPN Monitor
        </Typography>
      </div>
    );
  }
}

export default withStyles(styles)(Top);
