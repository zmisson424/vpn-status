const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const isDev = require('electron-is-dev');
const si = require('systeminformation');
const publicIp = require('public-ip');

const config = require('./config.json');

let mainWindow;

function setGlobalVariables() {
  // Basic Network Settings
  si.networkInterfaces()
    .then(data => {
      var proxyObj = {};
      var proxyCount = 0;
      for (var x in data) {
        if (data[x].iface === config.localIface) {
          // Handle local IP Settings
          global.ip = data[x].ip4;
          global.mac = data[x].mac;
        } else if (data[x].iface.substring(0, 3) === 'tun') {
          // Handle VPN/Proxy
          // tun for tunneling
          proxyObj[proxyCount] = {
            ip: data[x].ip4,
            mac: data[x].mac
          };
          proxyCount++;
        }
      }
      if (Object.keys(proxyObj).length > 0) {
        global.proxy = proxyObj;
      } else {
        global.proxy = 'Not Found';
      }
    })
    .catch(error => {
      global.ip = 'Not Found';
      global.mac = 'Not Found';
      global.proxy = 'Not Found';
      console.log('Error: Network Interfaces: ', error);
    });

  // Get public facing IP
  publicIp
    .v4()
    .then(data => {
      global.publicIp = data;
    })
    .catch(error => {
      global.publicIp = 'Not Found';
      console.log('Error. Public IP: ', error);
    });

  // Get Download / Upload Speeds

  // Get Location
}

function createWindow() {
  setGlobalVariables();
  mainWindow = new BrowserWindow({ width: 450, height: 600 });
  mainWindow.setMenu(null);
  mainWindow.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`
  );
  mainWindow.on('closed', () => (mainWindow = null));
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
